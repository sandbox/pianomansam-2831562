/**
 * @file
 * Default date values.
 */

(function ($, Drupal) {

  'use strict';

  Drupal.behaviors.datetimeTweaksDefaultDate = {
    attach: function (context, settings) {
      var $context = $(context);
      // Skip if date is supported by the browser.
      if (Modernizr.inputtypes.date === true) {
        return;
      }
      $context.find('input[data-drupal-date-format]').once('default-date').each(function () {
        var $el = $(this), val;
        var fmt = new DateFormatter();
        val = $el.val();
        if (val) {
          console.log(new Date(val));
          $el.val(fmt.formatDate(new Date(val), drupalSettings.datetime_tweaks.format));
        }

        var attributes = ['min', 'max'], i, field;
        for (i in attributes) {
          if (attributes.hasOwnProperty(i)) {
            field = attributes[i];
            val = $el.attr(field);
            if (val) {
              $el.attr(field, fmt.formatDate(new Date(val), drupalSettings.datetime_tweaks.format));
            }
          }
        }
      });
    }
  };

})(jQuery, Drupal);
