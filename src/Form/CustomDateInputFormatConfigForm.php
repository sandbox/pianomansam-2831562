<?php

namespace Drupal\custom_date_input_format\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Class CustomDateInputFormatConfigForm.
 *
 * @package Drupal\custom_date_input_format\Form
 */
class CustomDateInputFormatConfigForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      'custom_date_input_format.settings',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'custom_date_input_format_config_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('custom_date_input_format.settings');
    $form['format'] = array(
      '#type' => 'textfield',
      '#title' => $this->t('Format'),
      '#default_value' => $config->get('format'),
    );
    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    parent::validateForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    parent::submitForm($form, $form_state);

    $values = $form_state->getValues();
    $this->config('custom_date_input_format.settings')
      ->set('format', $values['format'])
      ->save();
  }

}
